import java.util.ArrayDeque;


/**
 * 利用了栈的思想，先进后出，完成抵消最后完全出栈完毕，也完成了匹配
 */
public class IsValid {
    public static void main(String[] args) {
//        String s = "(((())))";
//        System.out.println(isValid(s));

        //======================================
        String s1 = "((({{{[[[]]]}}})))";
        System.out.println(isValid2(s1));
    }

    /**
     * 只有一种括号的场景
     * @param s
     * @return
     * 时间复杂度：O(n)
     * 空间复杂读：O(n)
     */
    public static boolean isValid(String s){
        if(s.length() % 2 == 1)return false;
        ArrayDeque<Character> stack = new ArrayDeque<>();
        for (char c : s.toCharArray()){
            if(c == '('){
                //入栈
                stack.push(c);
            }else{
                if(stack.isEmpty())return false;
                //出栈
                stack.pop();
            }
        }
        return stack.isEmpty();
    }

    /**
     * 3种括号的场景
     * @param s
     * @return
     * 时间复杂度：O(n)
     * 空间复杂读：O(n)
     */
    public static boolean isValid2(String s){
        if(s.length() % 2 == 1) return false;
        ArrayDeque<Character> stack = new ArrayDeque<>();
        for(char c : s.toCharArray()){
            if(c == '(' || c == '{' || c == '['){
                stack.push(c);
            }else{
                if(stack.isEmpty())return false;
                char top = stack.pop();
                if(c == ')' && top != '(')return false;
                if(c == '}' && top != '{')return false;
                if(c == ']' && top != '[')return false;
            }
        }
        return stack.isEmpty();
    }
}
