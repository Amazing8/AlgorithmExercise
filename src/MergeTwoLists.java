public class MergeTwoLists {
    public static void main(String[] args) {
        LinkList head2 = new LinkList();
        head2.insert(2);
        head2.insert(5);
        head2.insert(8);
        head2.insert(10);
        LinkList head1 = new LinkList();
        head1.insert(1);
        head1.insert(4);
        head1.insert(7);
        head1.insert(9);
        System.out.println("合并之前输出两个有序链表");
        System.out.println("链表1");
        head1.printLink();
        System.out.println(" ");
        System.out.println("链表2");
        head2.printLink();
        System.out.println(" ");
        LinkList x = new LinkList();
        x.hebing(head1.head, head2.head);
        System.out.println("合并之后输出合并好的有序链表");
        x.printLink();
    }
}

