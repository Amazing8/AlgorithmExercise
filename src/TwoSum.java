import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class TwoSum {
    public static void main(String[] args) {
        int[] arr = sum(new int[]{1,5,6,8,0,9},13);
        System.out.println(Arrays.toString(arr));
//        for(int i = 0; i < arr.length; i++){
//            System.out.println(arr[i]);
//        }
    }

    /**
     * 两数之和[只有一组数据符合答案]
     */
    public static int[] sum(int[] arr,int target){
        Map<Integer,Integer> map = new HashMap<>();
        for(int i = 0; i < arr.length; i++){
            if(map.containsKey(target - arr[i])){
                return new int[]{map.get(target - arr[i]),i};
            }else{
                map.put(arr[i],i);
            }
        }
        return null;
    }
}
