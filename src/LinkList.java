public class LinkList {
    ListNode head = null;

    // 写一个方法，创建链表，链表的插入方法
    // 尾插法
    public void insert(int value) {
        ListNode listNode = new ListNode(value);
        if (head == null){
            head = listNode;
            return;
        }
        ListNode tempNode = head;
        while (tempNode.next !=null){
            tempNode = tempNode.next;
        }
        tempNode.next = listNode;
    }

    // 输出链表的值
    public void printLink() {
        //定义右边指向链表当中的第一个节点
        ListNode tempNode = head;
        while (tempNode !=null){
            System.out.print(tempNode.val);
            tempNode = tempNode.next;
        }
    }

    //合并两个有序链表
    public void hebing(ListNode head1, ListNode head2) {
        ListNode headNode = new ListNode(0);
        ListNode tempNode = headNode;
        while (head1 != null && head2 != null) {
            if (head1.val < head2.val) {
                tempNode.next = head1;
                head1 = head1.next;
            } else {
                tempNode.next = head2;
                head2 = head2.next;
            }
            tempNode = tempNode.next;
        }
        if (head1 == null) {
            tempNode.next = head2;
        }
        if (head2 == null) {
            tempNode.next = head1;
        }
        head = headNode.next;
    }
}
